#!/bin/bash

## BARE METAL Backup Script
## (c) Mack - Philip Henzler
## 1.0 
## 30.11.2011

#HOSTNAME=`hostname`
#HOSTNAME=mnet

TMPDIR=/tmp/backup/
/bin/mkdir -p $TMPDIR


TMPFILE_INC=$TMPDIR/bmbackup_include
TMPFILE_EXC=$TMPDIR/bmbackup_exclude

BASEDIR=/media/backup/cfg_backup
LOGPATH=$BASEDIR/log
BACKUPPATH=$BASEDIR/rdiff

#VMS=( pxen mkvm mnet mbkp map mex mon mfs )
#VMS=( mkvm mnet pxen mbkp mon map mex)
#VMS=( mon map mex mnet mkvm )
VMS=( $1 )

declare -i RESULT=0
declare -A errors

#create folders if needed
/bin/mkdir -p $LOGPATH
/bin/mkdir -p $BACKUPPATH


echo === Creating $TMPFILE ====
echo "/root/" > $TMPFILE_INC
echo "/etc/"  >> $TMPFILE_INC
echo "/srv/"  >> $TMPFILE_INC
echo "/home/" >> $TMPFILE_INC
echo "/var/" >> $TMPFILE_INC
echo $TMPDIR >> $TMPFILE_INC

echo "/var/backups" > $TMPFILE_EXC
echo "/var/cache/apt" >> $TMPFILE_EXC
echo "/var/cache/munin" >> $TMPFILE_EXC
echo "/etc/apparmor.d" >> $TMPFILE_EXC

# Don't backup MYSQL - big and on mex already double-saved
echo "/var/lib/mysql" >> $TMPFILE_EXC
echo "/var/lib/nagios3/spool" >> $TMPFILE_EXC

echo -----------------[ Include Dirs ]-------------------
cat $TMPFILE_INC
echo ""

echo -----------------[ Exclude Dirs ]-------------------
cat $TMPFILE_EXC
echo ""

# Fehler Array mit 0 füllen
    
	
echo --------------[ Beginning ]---------------
echo `date`

echo ------------[ Writing List of Packages ]-------------
#dpkg --get-selections > /root/installed_packs
dpkg --get-selections > /tmp/backup/installed_packs

#echo === Deleting old Increments
echo ------------[ Delete older Backups ]-------------
/usr/bin/rdiff-backup --force --remove-older-than 10B $BACKUPPATH >>$LOGPATH/bkp_log_$CURRVM 2>&1

echo -------------[ Running rdiff-backup ]---------------

/usr/bin/rdiff-backup --print-statistics  --exclude-globbing-filelist $TMPFILE_EXC --include-globbing-filelist $TMPFILE_INC --exclude / / $BACKUPPATH >>$LOGPATH/bkp_config.log 2>&1
echo ""
echo ""
echo ""

echo `date`
echo --------------[ Finished ]---------------
echo ""
echo ""
